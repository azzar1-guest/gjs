#!/bin/sh

: "${DEB_HOST_ARCH:=$(dpkg --print-architecture)}"

if "./gjs-console" -c 'print("Hello, world")'; then
	echo "Smoke-test successful, continuing with full test suite"
else
	echo "Smoke-test failed: did interpreter initialization fail? (see #873778)"
	exit 1
fi

if make -k check VERBOSE=1; then
	echo "check successful"
else
	echo "check failed, checking whether to continue anyway..."

	case "${DEB_HOST_ARCH}" in
		(s390x|ppc64|sparc64)
			echo "Ignoring test failure, https://bugs.debian.org/878286"
			;;
		(*)
			echo "Test failure is considered serious, causing FTBFS"
			exit 1
	esac
fi
